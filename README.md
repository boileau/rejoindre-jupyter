# Rejoindre Jupyter

## Installer les dépendances

Avec pip :

``` bash
pip install -r requirements.txt
```

## Exécuter le notebook

```bash
jupyter-notebook rejoindre-jupyter.ipynb
```

## Convertir en html

``` bash
make
```

## Tester avec binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.unistra.fr%2Fboileau%2Frejoindre-jupyter/master?filepath=rejoindre-jupyter.ipynb)
