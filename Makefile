notebook := rejoindre-jupyter

.PHONY = all clean images public

all: public/$(notebook).html public/index.html public images

public/index.html: $(notebook).ipynb public
	jupyter nbconvert --execute --to slides $<
	mv $(notebook).slides.html $@

public/%.html: %.ipynb public
	jupyter nbconvert --to html --template classic --execute $< --output $@

public:
	mkdir -p public

images: public
	rm -rf public/images
	cp -r images public/
	mv public/images/favicon.ico public/

clean:
	rm -rf public